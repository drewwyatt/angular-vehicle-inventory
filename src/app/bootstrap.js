'use strict';

import app from './app';

angular.element(document).ready(function() {
  angular.bootstrap(document, [app.name], { strictDi: true });
});

export function bootstrap() {};
