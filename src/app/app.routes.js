'use strict';

routes.$inject = ['$stateProvider', '$urlRouterProvider'];
function routes($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('inventory', {
      url: '/',
      templateUrl: 'app/components/inventory/inventory.html',
      controller: 'InventoryController as vm'
    })
    .state('reservation', {
      url: '/reservation',
      templateUrl: 'app/components/reservation/reservation.html',
      controller: 'ReservationController as vm'
    });

  $urlRouterProvider.otherwise('/');
}

export default routes;
