'use strict';

import './lib/vendors';
import register from './lib/register';
import routes from './app.routes';

import * as inventory from './components/inventory/inventory.module';
import * as reservation from './components/reservation/reservation.module';
import * as shared from './components/shared/shared.module';

angular.module('app', ['ui.router', 'matchMedia', 'ngMessages', 'app.components.inventory', 'app.components.reservation', 'app.components.shared'])
  .config(routes);

angular.module('app.components.inventory', []);
angular.module('app.components.reservation', []);
angular.module('app.components.shared', []);

register('app.components.inventory')
  .controller('InventoryController', inventory.controller)
  .directive('inventoryList', inventory.directives.list)
  .directive('inventoryTable', inventory.directives.table)
  .service('InventoryVehicleService', inventory.service);

register('app.components.reservation')
  .controller('ReservationController', reservation.controller);

register('app.components.shared')
  .directive('spinner', shared.directives.spinner)
  .factory('$localstorage', shared.factories.$localstorage)
  .factory('BoatFactory', shared.factories.boat)
  .factory('CarFactory', shared.factories.car)
  .factory('TruckFactory', shared.factories.truck)
  .service('URLService', shared.services.url);

export default angular.module('app');
