'use strict';

class ReservationController {
  constructor($localstorage, $state) {
    this.$localstorage = $localstorage;
    this.$state = $state;

    this.init();
  }

  init() {
    this.getSelectedVehicle()
      .then((vehicle) => {
        this.vehicle = vehicle;
      })
      .catch(() => {
        this.$state.go('inventory');
      });
  }

  getSelectedVehicle() {
    let promise = new Promise((resolve, reject) => {
      let vehicle = this.$localstorage.getObject('SELECTED_VEHICLE');
    if(vehicle) {
        resolve(vehicle);
      } else {
        reject();
      }
    });

    return promise;
  }

  reserve() {
    this.$localstorage.unset('SELECTED_VEHICLE');
    this.$localstorage.setObject('RESERVED_VEHICLE', this.vehicle);
    this.$state.go('inventory');
  }
}

ReservationController.$inject = ['$localstorage', '$state'];
export default ReservationController;
