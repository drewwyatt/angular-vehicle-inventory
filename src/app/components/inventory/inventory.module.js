'use strict';

import InventoryController from './inventory.controller';
import InventoryListDirective from './inventory.list.directive';
import InventoryTableDirective from './inventory.table.directive';
import InventoryVehicleService from './inventory.vehicle.service';

const controller = InventoryController;
const directives = { list: InventoryListDirective, table: InventoryTableDirective };
const service = InventoryVehicleService;

export { controller };
export { directives };
export { service };
