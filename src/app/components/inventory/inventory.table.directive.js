'use strict';

class InventoryTableDirective {
  constructor($timeout) {
    this.scope = {
      data: '=',
      onReserve: '&'
    };
    this.templateUrl = 'app/components/inventory/inventory.table.directive.template.html';

    this.init($timeout);
  }

  init($timeout) {
    this.$timeout = $timeout;
  }

  link(scope, element) {
    scope.columns = {};
    scope.$watch('data', (newData, oldData) => {
      if (newData) {
        let vehicle = scope.data[0];
        for (let property in vehicle) {
          if (vehicle.hasOwnProperty(property)) {
            scope.columns[property] = true;
          }
        }
      }
    });

    scope.reserve = (oid, type) => {
      scope.onReserve({
        oid, type
      });
    }
  }
}

InventoryTableDirective.$inject = ['$timeout'];
export default InventoryTableDirective;
