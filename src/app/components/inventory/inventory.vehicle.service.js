'use strict';

class VehicleService {
  constructor($http, $localstorage, Boat, Car, Truck, URL) {
    /**
     * Assign dependencies to instance properties
     * so that they can be accessed in instance methods
     */

    this.$http = $http;
    this.$localstorage = $localstorage;
    this.url = URL;

    this.factories = {
      boat: Boat,
      car: Car,
      truck: Truck
    };
    this.type = {
      boat: 'boat',
      car: 'car',
      truck: 'truck'
    };
  }

  fetch(type) {
    /**
     * Returns a promise that will resolve with vehicle data
     *
     * Caching is used for the purpose of spoofing a database.
     * Using the cache methods detailed below allow this service to
     * provide the illusion of making a reservation (for two minutes).
     */

    let promise = new Promise((resolve, reject) => {
      if (this.type[type] === undefined) {
        throw new TypeError(`Type argument "${type}" is not a supported vehicle type.`);
      }

      this.fetchFromCache(type)
        .then((vehicles) => {
          // Returns the vehicles from $localstorage
          resolve(vehicles);
        })
        .catch(() => {
          // Fetches vehicles from API
          // (and caches them)

          this.$http.get(this.url.endpoint(type))
            .then((response) => {
              let vehicles = [];
              response.data.forEach((vehicle) => {
                vehicles.push(this.factories[type].build(vehicle));
              });

              this.cache(type, vehicles);
              resolve(vehicles);
            }, (error) => {
              reject(error);
            });
        });
    });

    return promise;
  }

  update(type, vehicle) {
    /**
     * Updates the cache so that future calls to this service
     * reflect changes made to this vehicle.
     *
     * This function retrieves the cache, finds the updated vehicle,
     * assigns the vehicle passed to this function to the old value in the array
     * then recaches the array.
     */

    this.fetchFromCache(type)
      .then((vehicles) => {
        let index;
        vehicles.forEach((v, i) => {
          if (v.oid === vehicle.oid) {
            index = i;
          }
        });

        if(index !== undefined) {
            vehicles[index] = vehicle;
            this.cache(type, vehicles);
        }
      });
  }


  cache(type, payload) {
    /**
     * Saves the payload passed to the array in $localstorage,
     * using the type as part of the key.
     *
     * Assigns Date object to timeproperty of the saved object
     * so that it can be expired later.
     */

    let cacheObject = {
      payload, time: new Date()
    };
    this.$localstorage.setObject(`CACHED_${type}`, cacheObject);
  }

  fetchFromCache(type) {
    /**
     * Returns promise that resolves with data if a valid cache object
     * is found. Otherwise rejects.
     *
     * This function checks to see if anything exists in $localstorage.
     * If something is found, this checks to see if the cached timestamp
     * is older than two minutes (by adding two minutes to is, then comparing it to now).
     * If two minutes have not elapsed - the promise is resolved with the cached payload.
     *
     * If any of the requirements detailed ^above^ are note met, the promise is rejected.
     */

    let promise = new Promise((resolve, reject) => {
      let data = this.$localstorage.getObject(`CACHED_${type}`);
      if (data) {
        let cacheTimePlusTwo = new Date(data.time);
        cacheTimePlusTwo.setMinutes(cacheTimePlusTwo.getMinutes() + 2);
        if (cacheTimePlusTwo > new Date()) {
          resolve(data.payload);
        } else {
          this.$localstorage.unset(`CACHED_${type}`);
          reject();
        }
      } else {
        reject();
      }
    });

    return promise;
  }
}

VehicleService.$inject = ['$http', '$localstorage', 'BoatFactory', 'CarFactory', 'TruckFactory', 'URLService'];

export default VehicleService;
