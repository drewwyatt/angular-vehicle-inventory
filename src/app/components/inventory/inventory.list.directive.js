'use strict';

class InventoryListDirective {
  constructor() {
    this.templateUrl = 'app/components/inventory/inventory.list.directive.template.html';
    this.scope = {
      data: '=',
      onReserve: '&'
    }
  }

  link(scope) {
    scope.reserve = (oid, type) => {
      scope.onReserve({ oid, type });
    }
  }
}

export default InventoryListDirective;
