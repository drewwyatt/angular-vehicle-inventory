'use strict';

class InventoryController {
  constructor($localstorage, $scope, $state, $window, screenSize, VehicleService) {
    /**
     * Assign dependencies to instance properties
     * so that they can be accessed in instance methods
     */

    this.$localstorage = $localstorage;
    this.$scope = $scope;
    this.$state = $state;
    this.$window = $window;
    this.screensize = screenSize;
    this.vehicles = VehicleService;

    /**
     * Run startup tasks
     */

    this.init();
  }

  init() {
    // Get vehicle data
    this.fetch()
      .then(() => {
        // Mark vehicles as reserved if there are any
        this.completePendingReservation();
      });

    // Sets desktop/mobile view flag
    this.setLayout();
    // Reruns ^setLayout^ if window is resized
    this.setResizeListener();
  }

  fetch() {
    /**
     * Returns a promise that resolves after all vehicles are loaded
     */
    let promise = new Promise((resolve, reject) => {
      Promise.all([
        this.load(this.vehicles.type.boat, 'boats'),
        this.load(this.vehicles.type.car, 'cars'),
        this.load(this.vehicles.type.truck, 'trucks')
      ]).then(() => {
        resolve();
      });
    });

    return promise;
  }

  load(type, prop) {
    /**
     * Returns a promise that resolves after VehicleService
     * has returned data and they have been loaded into named properties
     * e.g. this.boats
     */
    let promise = new Promise((resolve, reject) => {
      this.vehicles.fetch(type)
        .then((vehicles) => {
          this.$scope.$apply(() => {
            this[prop] = vehicles;
            resolve();
          });
        })
        .catch((err) => {
          console.log(type, prop, err);
        });
    });

    return promise;
  }

  find(oid, type) {
    /**
     * Retuens a promise that resolves with a vehicle after
     * that vehicle has been found using an oid and a type.
     *
     * Type is required because vehicle datasets are divided into
     * named properties (e.g. this.boats). Type prevents this function
     * from having to search in all possible datasets.
     */

    let promise = new Promise((resolve, reject) => {
      let vehicle;

      this.getVehicleSetWithType(type)
        .then((vehicles) => {
          vehicle = vehicles.filter((v) => {
            return v.oid === oid
          });

          if (vehicle.length === 1) {
            resolve(vehicle[0]);
          } else {
            reject();
          }
        });
    });
    return promise;
  }

  reserve(oid, type) {
    /**
     * Finds a vehicle, queues it by saving it to localstorage,
     * then navigates to reservation.
     */

    this.find(oid, type)
      .then((vehicle) => {
        this.$localstorage.setObject('SELECTED_VEHICLE', vehicle);
        this.$state.go('reservation');
      })
      .catch(() => {
        alert('Uh oh. There was a problem...');
      });
  }

  completePendingReservation() {
    /**
     * Checks for a vehicle saved in localstorage.
     * If one is found, retrieves a vehicle set using the vehicleType property.
     * Then searches dataset for a vehicle with a matching oid.
     * If one is found, the local "reserved" property on that vehicle is set to
     * true, the vehicle is removed from localstorage queue, and a call is made
     * to the update function on the VehicleService. (which will spoof a database update)
     */

    const vehicle = this.$localstorage.getObject('RESERVED_VEHICLE');
    if (vehicle) {
      this.getVehicleSetWithType(vehicle.vehicleType)
        .then((vehicles) => {
          vehicles.forEach((v) => {
            if (v.oid === vehicle.oid) {
              this.$scope.$apply(() => {
                v.reserved = true; // update local copy
                this.$localstorage.unset('RESERVED_VEHICLE'); // remove from queue
                this.vehicles.update(vehicle.vehicleType, v); // propagate change
              });
            }
          });
        });
    }
  }

  getVehicleSetWithType(type) {
    /**
     * Returns a promise that will resolve with a named vehicle set
     * that matches the type passed to this funciton.
     */

    let promise = new Promise((resolve, reject) => {
      switch (type) {
        case 'boat':
          resolve(this.boats);
          break;
        case 'car':
          resolve(this.cars);
          break;
        case 'truck':
          resolve(this.trucks);
          break;
        default:
          reject(new TypeError(`Vehicle type ${type} not recognized.`));
          break;
      }
    });

    return promise;
  }

  setResizeListener() {
    /**
     * Calls setLayout every time the window is resized.
     */

    let w = angular.element(this.$window);

    w.bind('resize', () => {
      this.$scope.$apply(() => {
        this.setLayout();
      });
    });
  }

  setLayout() {
    /**
     * Sets a boolean value to true if window size is larger than ___.
     */

    if (this.screensize.is('xs')) {
      this.desktop = false;
    } else {
      this.desktop = true;
    }
  }
}

InventoryController.$inject = ['$localstorage', '$scope', '$state', '$window', 'screenSize', 'InventoryVehicleService'];
export default InventoryController;
