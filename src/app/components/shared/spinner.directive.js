'use strict';

class SpinnerDirective {
  constructor() {
    this.templateUrl = 'app/components/shared/spinner.directive.template.svg';
  }
}

export default SpinnerDirective;
