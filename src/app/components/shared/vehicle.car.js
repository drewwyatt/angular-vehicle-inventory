'use strict';

import Vehicle from './vehicle';

class Car extends Vehicle {
  constructor(payload) {
    super(payload);

    this.vehicleType = 'car';
    this.mileage = payload.mileage;
    this.doors = payload.doors;
    this.horsepower = payload.horsepower;
  }
}

class CarFactory {
  constructor() {}

  build(payload) {
    return new Car(payload);
  }
}

export default CarFactory;
