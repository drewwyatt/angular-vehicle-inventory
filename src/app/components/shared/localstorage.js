'use strict';

class $localstorage {
  constructor($window) {
    this.$window = $window;
  }

  get(key) {
    return this.$window.localStorage[key];
  }

  set(key, value) {
    this.$window.localStorage[key] = value;
  }

  getObject(key) {
    const val = this.$window.localStorage[key];
    if(val) {
      return JSON.parse(val);
    }

    return undefined;
  }

  setObject(key, value) {
    this.$window.localStorage[key] = JSON.stringify(value);
  }

  unset(key) {
    delete this.$window.localStorage[key];
  }
}

$localstorage.$inject = ['$window'];
export default $localstorage;
