'use strict';

import $localstorage from './localstorage';
import BoatFactory from './vehicle.boat';
import CarFactory from './vehicle.car';
import SpinnerDirective from './spinner.directive';
import TruckFactory from './vehicle.truck';
import URLService from './url.service';

const directives = { spinner: SpinnerDirective };
const factories = { $localstorage, boat: BoatFactory, car: CarFactory, truck: TruckFactory };
const services = { url: URLService };

export { directives };
export { factories };
export { services };
