'use strict';

class URLService {
  constructor() {
    this.init();
  }

  init() {
    this.host = 'sandbox.bottlerocketapps.com';
    // this.host = 'localhost:3000';
    this.paths = {
        boat: '/FrontEndTechExam2015/boats.json',
        car: '/FrontEndTechExam2015/cars.json',
        truck: '/FrontEndTechExam2015/trucks.json'
    };
  }

  get base() {
    return `//${this.host}`;
  }

  endpoint(resource) {
    if(this.paths[resource] === undefined) {
      throw new TypeError(`Endpoint argument "${resource}" is not a supported resource type.`);
    }

    return `//${this.host}${this.paths[resource]}`;
  }
}

export default URLService;
