'use strict';

import Vehicle from './vehicle';

class Boat extends Vehicle {
  constructor(payload) {
    super(payload);

    this.vehicleType = 'boat';
    this.engineHours = payload.engineHours;
    this.type = payload.type;
    this.price = payload.price;
  }
}

class BoatFactory {
  constructor() {}

  build(payload) {
    return new Boat(payload);
  }
}

export default BoatFactory;
