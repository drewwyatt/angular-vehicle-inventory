'use strict';

import Vehicle from './vehicle';

class Truck extends Vehicle {
  constructor(payload) {
    super(payload);

    this.vehicleType = 'truck';
    this.mileage = payload.mileage;
    this.doors = payload.doors;
    this.horsepower = payload.horsepower;
  }
}

class TruckFactory {
  constructor() {}

  build(payload) {
    return new Truck(payload);
  }
}

export default TruckFactory;
