'use strict';

class Vehicle {
  constructor(payload) {
    this.checkForImplementation();

    this.oid = payload.oid;
    this.year = payload.year;
    this.make = payload.make;
    this.model = payload.model;
    this.color = payload.color;
    this.price = payload.price;
    this.reserved = false;
  }

  checkForImplementation() {
    if(this.constructor === Vehicle) {
      throw new TypeError('Abstract class "Vehicle" cannot be instantiated directly.');
    }
  }
}

export default Vehicle;
