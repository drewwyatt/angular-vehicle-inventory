require('es6-promise').polyfill();

const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const postcss = require('gulp-postcss');
const rename = require('gulp-rename');

const paths = {
  src: {
    css: 'src/css/main.css'
  },
  dest: {
    css: 'src/css/'
  },
  watchFiles: {
    css: 'src/**/*.css',
    html: 'src/**/*.html',
    js: 'src/**/*.js'
  }
}

gulp.task('default', ['css', 'browser-sync', 'watch']);

gulp.task('watch', function() {
  gulp.watch([paths.watchFiles.html, paths.watchFiles.js], [browserSync.reload]);
  gulp.watch(paths.watchFiles.css, ['css', browserSync.reload]);
});

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: ['src', 'jspm_packages', './']
    }
  });
});

gulp.task('css', function() {
  gulp.src(paths.src.css)
    .pipe(rename('app.css'))
    .pipe(postcss([require('postcss-import'), require('postcss-simple-vars'), require('postcss-nested')]))
    .pipe(gulp.dest(paths.dest.css));
});
