# ES6 Angular Vehicle Inventory

## tl;dr

To go from cloned to running locally, do:

````bash
$ npm install
$ jspm install
$ gulp
````

## Installation

This app uses [NPM](https://nodejs.org/en/), [JSPM](http://jspm.io/), and [Gulp](http://gulpjs.com/).

Once you have NPM installed (probably via node) run `npm install`. <-- That will install all node packages, including JSPM and Gulp, then you can run `jspm install` to pull down angular, and its dependencies.

#### About JSPM Packages

A complete list of packages that will be installed by JSPM can be found in the JSPM dependencies section of this app's package.json file. They are imported by the app using [ES6 import statements](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import) in **./src/app/lib/vendors.js**.

## Running this thing

After everything is installed, running `gulp` will compile all [PostCSS](https://github.com/postcss/postcss) (notice that /src/css/main.css acts only as a manifest - this builds app.css), then uses [BrowserSync](http://www.browsersync.io/) to open a web browser, and serve the app and its dependencies.

This command, also starts up a couple of watcher tasks, but they will only be triggered if the source is modified.

## Babel (via JSPM)

Some extra magic is happening behind the scenes. The ES6 is being transpiled into ES5 with [Babel](https://babeljs.io/ ) (via JSPM) - this is configured in the jspm.config.js file in the project root.

You can see where this is bootstrapped in the index.html file here:

````html
<script src="jspm_packages/system.js"></script>
<script src="jspm.config.js"></script>
<script>
  System.import('app/bootstrap.js').catch(console.error.bind(console));
</script>
````

BrowserSync helps out here a bit as well, by allowing us to treat multiple directories as the project root, which allowed me to keep the jspm_packages directory, and the config file, out of /src.
